# Haskell Physics Engine (WIP)

A physics engine written in Haskell


## Current Features

* Simulation of shapes under constant gravity or newtonian gravity (planetary motion) in 2D
* Some simple collision handling for 2D shapes
* 3D faking effect to visualise 3D motion


## Planned Features

* Proper collision handling
* More forces (Electrostatic, magnetic, fluid resistance, friction)
* UI (Currently experiments must be setup in Haskell)


## Requirements

* Gloss
* GHC

### Dev todo

1. Add ability to fix rotation or position or both (move check into integrator?)
2. Create more accurate collision resolvers
3. Create more efficient collision detectors
4. Make point & plane of collision detection work for multiple intersect points - Theoretically done but for some reason it's broken collisions
5. Make ensuring no intersect after collision more robust - instead of resetting positions only move them apart as little as neccesary in a direction along and proportional to their velocity vectors

