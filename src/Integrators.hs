module Integrators
( Timestep
, Integrator
, forwardEuler
, semiImplicitEuler
, traditionalVerlet
) where

import LinAlg3D
import PhysicsObjects
import Forces

type Timestep = Float
type Integrator = Timestep -> LinearAcceleration -> AngularAcceleration -> PhysicsObject -> PhysicsObject

--Forward Euler is bad as it's non-conservative, only provided for demonstrative purposes
forwardEuler :: Integrator
forwardEuler dt la aa pObj@PhysicsObject {position=p, rotation=r, velocity=v, angularVelocity=av}
  = pObj{ position=newP, prevPosition=p, rotation=newR
        , velocity=newV, prevRotation=r, angularVelocity=newAV}
  where dT = tV3 dt
        newV = v + (la * dT)
        newP = p + (v * dT)
        newAV = av + (aa * dT)
        newR = r + (r * dT)

--Error is order 1
semiImplicitEuler :: Integrator
semiImplicitEuler dt la aa pObj@PhysicsObject { position=p, rotation=r
                                              , velocity=v, angularVelocity=av}
  = pObj{ position=newP, prevPosition=p, rotation=newR
        , velocity=newV, prevRotation=r, angularVelocity=newAV}
  where dT = tV3 dt
        newV = v + (la * dT)
        newP = p + (newV * dT)
        newAV = av + (aa * dT)
        newR = r + (newAV * dT)

--Error is order 2, not valid if acceleration is dependant on velocity
traditionalVerlet :: Integrator
traditionalVerlet dt la aa pObj@PhysicsObject { position=p, prevPosition=pp, rotation=r
                                              , prevRotation=pr, velocity=v, angularVelocity=av}
  = pObj{ position=newP, prevPosition=p, rotation=newR
        , velocity=newV, prevRotation=r, angularVelocity=newAV}
  where dT = tV3 dt
        newP = if pp /= zeroVec then (2 * p) - pp + (la * dT * dT) else p + ((v + (la * dT)) * dT)
        newV = ((newP - p) / dT) + (0.5 * la * dT)
        newR = if pr /= zeroVec then (2 * r) - pr + (aa * dT * dT) else r + ((av + (aa * dT)) * dT)
        newAV = ((newR - r) / dT) + (0.5 * aa * dT)

