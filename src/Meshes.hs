module Meshes
( Mesh
, Point
, square
, rectangle
, box
, regularPolygon
, translateMesh
, rotateMesh
, intersections
) where

import LinAlg3D
import Util

--Whilst some things may seem to be copying Gloss, the purpose is to
--refactor things such that all objects are described by a series of lines.
--This will greatly aid 2D collision detection

type Point = Vec3 Float
type Edge = (Point,Point)

--NOTE, as we are working in 2D for collision all points in a mesh should have z=0
type Mesh = [Point]

meshFromList :: [(Float,Float)] -> Mesh
meshFromList [] = []
meshFromList ((x,y):as) = Vec3 (x, y, 0) : meshFromList as

box :: Float -> Float -> Mesh
box x y = innerRec ++ [head innerRec] ++ outerRec ++ [head outerRec]
  where t = 20
        innerRec = rectangle x y
        outerRec = rectangle (x+t) (y+t)

square :: Float -> Mesh
square sideLength = rectangle sideLength sideLength

rectangle :: Float -> Float -> Mesh
rectangle x y = [Vec3 (hx,hy,0), Vec3 (hx,-hy,0), Vec3(-hx,-hy,0), Vec3(-hx,hy,0)]
  where hx = x / 2
        hy = y / 2

regularPolygon :: Int -> Float -> Mesh
regularPolygon n r = map rotatePoint [0..(n-1)]
  where rotatePoint i = Vec3 (r * sin theta, r * cos theta, 0)
          where k = (fromRational . toRational) i :: Float
                theta = k * sideAngle + offset
                offset = if even n then sideAngle / 2 else 0
        sideAngle = 2 * pi / (fromRational . toRational) n :: Float

translateMesh :: Float -> Float -> Mesh -> Mesh
translateMesh dx dy = map (\(Vec3 (x, y, z)) -> Vec3 (x+dx, y+dy, z))

rotateMesh :: Float -> Mesh -> Mesh
rotateMesh a = map rotatePoint
  where rotatePoint (Vec3 (x,y,z)) = Vec3 (x * cos a - y * sin a, x * sin a + y * cos a, z)

intersections :: Mesh -> Mesh -> [Point]
intersections m1 m2 = filterMaybeList [ edgeIntersection a b | a <- edges m1, b <- edges m2 ]

--Returns nothing if both lines are parallel or if edges don't intersect
edgeIntersection :: Edge -> Edge -> Maybe Point
edgeIntersection e1@(Vec3 (e1x1,_,_),_) e2@(Vec3 (e2x1,_,_),_)
  | linesParallel = Nothing
  | isInRange = Just (Vec3 (xCoord,yCoord,0))
  | otherwise = Nothing
  where m (Vec3 (x1,y1,_), Vec3 (x2,y2,_)) = (y2 - y1) / (x2 - x1) --Gradient of edge line
        c e@(Vec3 (x1,y1,_),_) = y1 - (m e * x1)  --Y intercept of edge line

        --Just to optimise incase ghc decides not to store values
        (m1, m2) = (m e1, m e2)
        (c1, c2) = (c e1, c e2)
        
        linesParallel = m1 == m2 || (isVertical e1 && isVertical e2)

        isInRange = inRange e1 && inRange e2
          where inRange (Vec3 (x1,y1,_), Vec3 (x2,y2,_)) = isBetween xCoord x1 x2 && isBetween yCoord y1 y2

        isVertical :: Edge -> Bool
        isVertical (Vec3 (x1,_,_), Vec3 (x2,_,_))
          | x1 == x2  = True
          | otherwise = False
        
        --x coordinate of edge intersection, need to protect against vertical lines
        xCoord :: Float
        xCoord
          | isVertical e1 = e1x1
          | isVertical e2 = e2x1
          | otherwise     = (c2 - c1) / (m1 - m2)
        
        --y coordinate of edge intersection, linesParallel protects against both vertical
        yCoord :: Float
        yCoord
          | isVertical e1 = (m2 * xCoord) + c2
          | otherwise     = (m1 * xCoord) + c1

edges :: Mesh -> [Edge]
edges m = edges' (m ++ [head m])
  where edges' [] = []
        edges' [a] = []
        edges' (a:b:as) = (a,b) : edges' (b:as)

{- -- LEGACY, not updated to used 3D vectors with z=0, still use (Float,Float)
pointInside :: Mesh -> Mesh -> Bool
pointInside a = any (`pointInPolygon` a)

pointInPolygon :: Point -> Mesh -> Bool
pointInPolygon p = odd . sum . map testEdge . edges
  where testEdge e = if rayCrossEdge p e then 1 else 0
        rayCrossEdge :: Point -> Edge -> Bool
        rayCrossEdge (p0x,p0y) ((p1x',p1y'),(p2x',p2y'))
          = not bothInNoCrossHalf && (bothRight || diagCheck)
          where (p1x,p1y) = (p1x' - p0x, p1y' - p0y)
                (p2x,p2y) = (p2x' - p0x, p2y' - p0y)
                bothLeft = p1x <= 0 && p2x <= 0
                bothRight = p1x >= 0 && p2x >= 0
                bothTopOrBottom = (p1y * p2y) >= 0
                bothInNoCrossHalf = bothLeft || bothTopOrBottom
                diagCheck = p1x - (p1y / m) > 0
                m = (p2y - p1y) / (p2x - p1x) -}

