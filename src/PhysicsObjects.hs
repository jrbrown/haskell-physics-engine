module PhysicsObjects
( PhysicsObject (..)
, Position
, Rotation
, Velocity
, AngularVelocity
, LinearAcceleration
, AngularAcceleration
, InertiaMatrix
, defaultPhysicsObject
) where

import LinAlg3D
import Meshes

--Useful type synonyms
type Position = Vec3 Float
type Rotation = Vec3 Float
type Velocity = Vec3 Float
type AngularVelocity = Vec3 Float
type LinearAcceleration = Vec3 Float
type AngularAcceleration = Vec3 Float
type InertiaMatrix = Mat3 Float

--Main data type that represents something in the world subject to physics
data PhysicsObject = PhysicsObject { pObjId :: String
                                   , dynamic :: Bool
                                   , mass :: Float
                                   , inertiaMatrix :: InertiaMatrix
                                   , position :: Position  --Center of Mass
                                   , prevPosition :: Position  --For traditional verlet
                                   , rotation :: Rotation
                                   , prevRotation :: Rotation
                                   , velocity :: Velocity
                                   , angularVelocity :: AngularVelocity
                                   , mesh :: Mesh}

instance Eq PhysicsObject where
  PhysicsObject{pObjId=a} == PhysicsObject{pObjId=b} = a == b

instance Show PhysicsObject where
  show PhysicsObject{ pObjId=n
                    , dynamic=d
                    , mass=m
                    , inertiaMatrix=im
                    , position=p
                    , rotation=r
                    , velocity=v
                    , angularVelocity=av}
    = "PhysicsObject, \"" ++ n ++ "\":" ++ 
      "\nDynamic           " ++ show d ++
      "\nMass:             " ++ show m ++
      "\nInertiaMatrix:    " ++ show im ++
      "\nPosition:         " ++ show p ++
      "\nRotation:         " ++ show r ++
      "\nVelocity:         " ++ show v ++
      "\nAngular Velocity: " ++ show av

defaultPhysicsObject = PhysicsObject { pObjId="NO ID"
                                     , dynamic=True
                                     , mass=0
                                     , inertiaMatrix=zeroMat
                                     , position=zeroVec
                                     , prevPosition=zeroVec
                                     , rotation=zeroVec
                                     , prevRotation=zeroVec
                                     , velocity=zeroVec
                                     , angularVelocity=zeroVec
                                     , mesh = [zeroVec]}

