module CollisionSchemes
( CollisionScheme
, noCollisions
, reverseVelocity
, reverseVelocityOld
, freezeOnCollision
, basicLinearMomentumConserver
) where

import LinAlg3D
import PhysicsObjects
import Meshes
import Util

type CollisionData = (PhysicsObject, PhysicsObject, [Point])
type CollisionScheme = [PhysicsObject] -> [PhysicsObject]
type CollisionDetector = [PhysicsObject] -> [CollisionData]
type CollisionResolver = CollisionData -> (PhysicsObject, PhysicsObject)

--Note that currently all other collision schemes assume 2D
noCollisions :: CollisionScheme
noCollisions pObjs = pObjs

collisionSchemeTemplate :: CollisionDetector -> CollisionResolver -> CollisionScheme
collisionSchemeTemplate detector resolver pObjs
  = newObjs
  where collidingObjPairs = detector pObjs
        resolvedObjs = flatten $ map resolver collidingObjPairs
        newObjs = getUnique $ resolvedObjs ++ pObjs --Need a better way of doing this that handles an object being involved in multiple collision events

basicDetector :: CollisionDetector
basicDetector pObjs = internalFunc pObjs []
  where internalFunc [] b = b
        internalFunc [a] b = b
        internalFunc (pObj:pObjs) collidingObjPairs = internalFunc pObjs collidingObjPairs'
          where collidingObjPairs' = newCollisionPairs ++ collidingObjPairs
                newCollisionPairs = [ (pObj, pObj2, ips) | pObj2 <- pObjs
                                    , let ips = intersectingCheck pObj pObj2, (not . null) ips]

reverseVelocityResolver :: CollisionResolver
reverseVelocityResolver ( pObj1@PhysicsObject{ position=p1, rotation=r1
                                             , prevPosition=pp1, prevRotation=pr1
                                             , velocity=lv1, angularVelocity=av1}
                        , pObj2@PhysicsObject{ position=p2, rotation=r2
                                             , prevPosition=pp2, prevRotation=pr2
                                             , velocity=lv2, angularVelocity=av2}, _)
  = ( pObj1{ position=pp1, rotation=pr1, prevPosition=p1, prevRotation=r1
           , velocity= -lv1, angularVelocity= -av1}
    , pObj2{ position=pp2, rotation=pr2, prevPosition=p2, prevRotation=r2
           , velocity= -lv2, angularVelocity= -av2})

--Currently incompatible with traditionalVerlet integration
--Possibly wrong
linearMomentumConserver :: CollisionResolver
linearMomentumConserver (a, b, []) = (a, b)
linearMomentumConserver (a, b, [c]) = (a, b)
linearMomentumConserver ( pObj1@PhysicsObject{ mass=m1, position=p1
                                             , prevPosition=pp1, prevRotation=pr1
                                             , velocity=lv1, dynamic=d1}
                        , pObj2@PhysicsObject{ mass=m2
                                             , prevPosition=pp2, prevRotation=pr2
                                             , velocity=lv2, dynamic=d2}
                        , ips@(dsahfsj:ip2:_) )
  | not d1 && not d2 = (pObj1, pObj2)  --Ignore collision of non-dynamic objects
  | not d1 && d2 = swap $ linearMomentumConserver (pObj2, pObj1, ips)
  | d1 && not d2 = (pObj1{position=pp1, rotation=pr1, velocity=newlv}, pObj2)
  | otherwise = ( pObj1{position=pp1, rotation=pr1, velocity=newlv1}
                , pObj2{position=pp2, rotation=pr2, velocity=newlv2})
    where swap (x,y) = (y,x)
          pointOfCollision = average ips
          collisionPlane = (grad2DtoVec . fst . lineOfFit2D) ips -- direction $ ip2 - ip1
          parav1 = collisionPlane * tV3 (dotProd collisionPlane lv1)
          perpv1 = lv1 - parav1
          parav2 = collisionPlane * tV3 (dotProd collisionPlane lv2)
          perpv2 = lv2 - parav2
          impulse = (2 * perpv1 * mv1 * mv2 * mv2 - 2 * perpv2 * mv2 * mv1 * mv1) / (mv1 * mv1 + mv2 * mv2)
          impulseWithStatic = 2 * perpv1 * mv1
          mv1 = tV3 m1
          mv2 = tV3 m2
          perpvNew = perpv1 - (impulseWithStatic / mv1)
          perpv1new = perpv1 - (impulse / mv1)
          perpv2new = perpv2 + (impulse / mv2)
          newlv = perpvNew + parav1
          newlv1 = perpv1new + parav1
          newlv2 = perpv2new + parav2

intersectingCheck :: PhysicsObject -> PhysicsObject -> [Point]
intersectingCheck pObj1@PhysicsObject{position=Vec3(x1,y1,_), rotation=Vec3(_,_,r1)}
                  pObj2@PhysicsObject{position=Vec3(x2,y2,_), rotation=Vec3(_,_,r2)}
  = intersections m1 m2
    where m1 = (translateMesh x1 y1 . rotateMesh r1 . mesh) pObj1
          m2 = (translateMesh x2 y2 . rotateMesh r2 . mesh) pObj2

reverseVelocity :: CollisionScheme
reverseVelocity = collisionSchemeTemplate basicDetector reverseVelocityResolver

basicLinearMomentumConserver :: CollisionScheme
basicLinearMomentumConserver = collisionSchemeTemplate basicDetector linearMomentumConserver

--The following functions are legacy and not part of the new system, kept for testing purposes
freezeOnCollision :: CollisionScheme
freezeOnCollision pObjs = map resolvePobj pObjs
  where resolvePobj pObj@PhysicsObject {prevPosition=pp, prevRotation=pr}
          = if isColliding pObj pObjs then pObj{position=pp, rotation=pr} else pObj

reverseVelocityOld :: CollisionScheme
reverseVelocityOld pObjs = map resolvePobj pObjs
  where resolvePobj pObj@PhysicsObject { position=p, rotation=r
                                       , prevPosition=pp, prevRotation=pr
                                       , velocity=lv, angularVelocity=av}
          = if isColliding pObj pObjs then pObj{ position=pp, rotation=pr
                                               , prevPosition=p, prevRotation=r
                                               , velocity= -lv, angularVelocity= -av} else pObj

isColliding :: PhysicsObject -> [PhysicsObject] -> Bool
isColliding pObj = any (\x -> (not . null) (intersectingCheck pObj x) && x /= pObj)

