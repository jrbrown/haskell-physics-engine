module Util
( getUnique
, flatten
, filterMaybeList
, isBetween
, boundedMaxOptim
) where

getUnique :: (Eq a) => [a] -> [a]
getUnique as = internalFunc as []
  where internalFunc :: (Eq a ) => [a] -> [a] -> [a]
        internalFunc [] bs = bs
        internalFunc (a:as) bs = if a `elem` bs then internalFunc as bs else internalFunc as (a:bs)

flatten :: [(a,a)] -> [a]
flatten [] = []
flatten ((x,y):xys) = x : y : flatten xys

filterMaybeList :: [Maybe a] -> [a]
filterMaybeList ((Just x):xs) = x : filterMaybeList xs
filterMaybeList (Nothing:xs) = filterMaybeList xs
filterMaybeList [] = []

isBetween :: (Ord a) => a -> a -> a -> Bool
isBetween val b1 b2 = ((b1 <= val) && (val <= b2)) || ((b2 <= val) && (val <= b1))

boundedMaxOptim :: (Fractional a, Ord a, Integral b) => (a -> Bool) -> a -> a -> b -> Maybe a
boundedMaxOptim f sval tol maxIter
  | f sval = bMO f (Just sval, Nothing) tol maxIter
  | otherwise = bMO f (Nothing, Just sval) tol maxIter
  where bMO :: (Fractional a, Ord a, Integral b) => (a -> Bool) -> (Maybe a, Maybe a) -> a -> b -> Maybe a

        -- Known bounds
        bMO f (Just lBound, Just uBound) tol maxIter
          | dif < tol || maxIter <= 0 = Just lBound
          | f val     = bMO f (Just val, Just uBound) tol (maxIter-1)
          | otherwise = bMO f (Just lBound, Just val) tol (maxIter-1)
          where val = (lBound + uBound) / 2
                dif = uBound - lBound

        -- Know lower bound only
        bMO f (Just lBound, Nothing) tol maxIter
          | maxIter <= 0 = Just lBound
          | f val     = bMO f (Just val, Nothing) tol (maxIter-1)
          | otherwise = bMO f (Just lBound, Just val) tol (maxIter-1)
          where val = lBound * 2

        -- Known upper bound only
        bMO f (Nothing, Just uBound) tol maxIter
          | maxIter <= 0 = Nothing
          | f val     = bMO f (Just val, Just uBound) tol (maxIter-1)
          | otherwise = bMO f (Nothing, Just val) tol (maxIter-1)
          where val = uBound / 2

        -- This shouldn't happen
        bMO f (Nothing, Nothing) tol maxIter = Nothing

