module LinAlg3D
( Vec3 (..)
, Mat3 (..)
, magnitude
, direction
, dotProd
, crossProd
, vecSum
, matFromTuple
, matMul
, transpose
, applyMat
, identity
, tV3
, zeroVec
, zeroMat
, planarInertia
, average
, rotateZ
, angle
, lineOfFit2D
, grad2DtoVec
) where

--Useful constants
zeroVec = Vec3 (0,0,0) :: Vec3 Float
zeroMat = Vec3 (zeroVec, zeroVec, zeroVec) :: Mat3 Float
planarInertia = Vec3 (zeroVec, zeroVec, Vec3 (0,0,1)) :: Mat3 Float

--3D Vectors
newtype Vec3 a = Vec3 (a, a, a) deriving (Eq)

instance (Show a) => Show (Vec3 a) where
    show (Vec3 (a, b, c)) = "[" ++ show a ++ "," ++ show b ++ "," ++ show c ++ "]"

instance (Num a) => Num (Vec3 a) where
    Vec3 (a1, a2, a3) + Vec3 (b1, b2, b3) = Vec3 (a1+b1, a2+b2, a3+b3)
    Vec3 (a1, a2, a3) - Vec3 (b1, b2, b3) = Vec3 (a1-b1, a2-b2, a3-b3)
    Vec3 (a1, a2, a3) * Vec3 (b1, b2, b3) = Vec3 (a1*b1, a2*b2, a3*b3)
    abs (Vec3 (a, b, c)) = Vec3 (abs a, abs b, abs c)
    signum (Vec3 (a, b, c)) = Vec3 (signum a, signum b, signum c)
    fromInteger a = Vec3 (fromInteger a, fromInteger a, fromInteger a)

instance (Fractional a) => Fractional (Vec3 a) where
    Vec3 (a1, a2, a3) / Vec3 (b1, b2, b3) = Vec3 (a1/b1, a2/b2, a3/b3)
    fromRational a = Vec3 (fromRational a, fromRational a, fromRational a)

instance Functor Vec3 where
    fmap f (Vec3 (a,b,c)) = Vec3 (f a, f b, f c)

--3x3 Matrices
type Mat3 a = Vec3 (Vec3 a)

showMat :: (Show a) => Mat3 a -> String
showMat (Vec3 (a, b, c)) = "[" ++ show a ++ "\n," ++ show b ++ "\n," ++ show c ++ "]"

magnitude :: (Floating a) => Vec3 a -> a
magnitude (Vec3 (a, b, c)) = sqrt ((a^2) + (b^2) + (c^2))

direction :: (Floating a, Real a) => Vec3 a -> Vec3 a
direction v = v / tV3 (magnitude v)

dotProd :: (Num a) => Vec3 a -> Vec3 a -> a
dotProd (Vec3 (a1, a2, a3)) (Vec3 (b1, b2, b3)) = (a1 * b1) + (a2 * b2) + (a3 * b3)

crossProd :: (Num a) => Vec3 a -> Vec3 a -> Vec3 a
crossProd (Vec3 (a1, a2, a3)) (Vec3 (b1, b2, b3)) = Vec3 ((a2*b3)-(a3*b2)
                                                         ,(a3*b1)-(a1*b3)
                                                         ,(a1*b2)-(a2*b1))

vecSum :: (Num a) => Vec3 a -> a
vecSum (Vec3 (a, b, c)) = a + b + c

average :: (Fractional a, Num a) => [Vec3 a] -> Vec3 a
average vs = average' vs (Vec3 (0,0,0)) 0
    where average' :: (Fractional a, Num a) => [Vec3 a] -> Vec3 a -> Int -> Vec3 a
          average' [] totalVec n = totalVec / (fromRational . toRational) n
          average' (vec:vecs) totalVec n = average' vecs (vec + totalVec) (n+1) 

rotateZ :: (Num a, Floating a) => Vec3 a -> a -> Vec3 a
rotateZ v x = applyMat rotMatrix v
    where rotMatrix = Vec3 (Vec3 (cos x, -(sin x), 0), Vec3 (sin x, cos x, 0), Vec3 (0,0,1))

angle :: (Floating a, Num a) => Vec3 a -> Vec3 a -> a
angle v1 v2 = acos $ dotProd v1 v2 / (magnitude v1 * magnitude v2)

lineOfFit2D :: (Num a, Fractional a, Floating a) => [Vec3 a] -> (a,a)
lineOfFit2D vs = (m,c)
  where xavr = (avr . map (\(Vec3 (x,_,_)) -> x)) vs
        yavr = (avr . map (\(Vec3 (_,y,_)) -> y)) vs
        avr xs = sum xs / (fromRational . toRational . length) xs
        m = (sum . map (\(Vec3 (x,y,_)) -> (x-xavr)*(y-yavr))) vs / (sum . map (\(Vec3 (x,_,_)) -> (x-xavr)**2)) vs
        c = yavr - (m * xavr)

grad2DtoVec :: (Num a, Floating a, Real a) => a -> Vec3 a
grad2DtoVec m = direction $ Vec3 (1,m,0)

--Inner tuples interpreted as rows of matrix
matFromTuple :: ((a, a, a), (a, a, a), (a, a, a)) -> Mat3 a
matFromTuple (a, b, c) = Vec3 (Vec3 a, Vec3 b, Vec3 c)

transpose :: Mat3 a -> Mat3 a
transpose (Vec3 (Vec3 (a1, a2, a3), Vec3 (b1, b2, b3), Vec3 (c1, c2, c3)))
    = Vec3 (Vec3 (a1, b1, c1), Vec3 (a2, b2, c2), Vec3 (a3, b3, c3))

matMul :: (Num a) => Mat3 a -> Mat3 a -> Mat3 a
matMul (Vec3 (a, b, c)) m = Vec3 (Vec3 (dotProd a d, dotProd a e, dotProd a f)
                                 ,Vec3 (dotProd b d, dotProd b e, dotProd b f)
                                 ,Vec3 (dotProd c d, dotProd c e, dotProd c f))
    where Vec3 (d, e, f) = transpose m

applyMat :: (Num a) => Mat3 a -> Vec3 a -> Vec3 a
applyMat (Vec3 (a, b, c)) v = Vec3 (dotProd v a, dotProd v b, dotProd v c)

identity = matFromTuple ((1,0,0),(0,1,0),(0,0,1))

tV3 :: (Fractional a, Real a) => a -> Vec3 a
tV3 = fromRational . toRational

