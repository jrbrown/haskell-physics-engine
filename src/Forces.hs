module Forces
( Force
, ForceField
, constGravity
, newtonianGravity
) where

import LinAlg3D
import PhysicsObjects

type Force = Vec3 Float
type ForceField = [PhysicsObject] -> PhysicsObject -> (Force, Position)

constGravity :: ForceField
constGravity _ pObj = (down * tV3 (m * g), zeroVec)
  where down = Vec3 (0, -1, 0)
        g = 100  --This planet has stronk gravity
        m = mass pObj

newtonianGravity :: ForceField
newtonianGravity pObjs pObj
  = (totalForce, zeroVec)
  where g = 10000000
        totalForce = sum (map getForce pObjs)
        getForce x = if pObj /= x then direction relativePosition * tV3 forceMagnitude else 0
          where forceMagnitude = g * (mass pObj * mass x) / magnitude relativePosition ^ 2
                relativePosition = position x - position pObj

