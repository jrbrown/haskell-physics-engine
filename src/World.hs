module World
( World
, testWorld
, drawWorld
, updateWorld
) where

import LinAlg3D
import PhysicsObjects
import Forces
import Integrators
import CollisionSchemes
import Meshes
import Graphics.Gloss

type WorldUpdater = Timestep -> World -> World
type World = ([PhysicsObject], [ForceField])

accelerationCalculator :: World -> PhysicsObject -> (LinearAcceleration, AngularAcceleration)
accelerationCalculator (pObjs, forceFields) pObj = if m /= 0 then (la, aa) else (0,0)
  where forcePosPairs = [f pObjs pObj | f <- forceFields]
        la = sum [force / m | (force, pos) <- forcePosPairs]
        m = (tV3 . mass) pObj
        --Note, angular acceleration calculation is untested and may be incorrect!
        aa = sum (map angularAccelerationCalc forcePosPairs)
        angularAccelerationCalc (force, pos) = torque / tV3 scalarMoI
            where torque = crossProd force pos
                  scalarMoI = dotProd pDir (applyMat (inertiaMatrix pObj) pDir)
                  pDir = direction pos

worldUpdaterTemplate :: Integrator -> CollisionScheme -> WorldUpdater
worldUpdaterTemplate integrator collisionscheme dt world@(pObjs, forceFields)
  = (collisionscheme intermediatePObjs, forceFields)
  where intermediatePObjs :: [PhysicsObject]
        intermediatePObjs = map integratorUpdate pObjs
        integratorUpdate :: PhysicsObject -> PhysicsObject
        integratorUpdate x = if dynamic x then uncurry (integrator dt) (accelerations x) x else x
        accelerations x = accelerationCalculator world x

updateWorld :: WorldUpdater
updateWorld = worldUpdaterTemplate semiImplicitEuler basicLinearMomentumConserver

drawWorld :: World -> Picture
drawWorld (pObjs, _) = Pictures $ map drawPhysicsObject pObjs

drawPhysicsObject :: PhysicsObject -> Picture
drawPhysicsObject pObj@PhysicsObject{position=Vec3 (x, y, z), rotation=Vec3 (_, _, r)}
  = (scale k k . translate x y . rotate (r * 180 / pi) . makePicture . make2D . mesh) pObj
    where make2D vs = map (\(Vec3 (x,y,_)) -> (x,y)) vs
          makePicture points = if fillObjs then Polygon points else Line (points ++ [head points])
          k = if applyFake3D then fake3D z else 1
          applyFake3D = False
          fillObjs = False

--Some test stuff, will move this to its own file / folder

--Don't use 3D with collisions, it's wonky
fake3D :: (Num a, Ord a, Fractional a) => a -> a
fake3D z = if distToCamera > 0 then focalLength / distToCamera else 0
  where focalLength = 1000
        distToCamera = 1000 + z

testWorld = (boundBox : ballGrid 3 3 200 200 (-100) (-100) 100 4, [])

random :: (Num a, Fractional a) => (a,a) -> Int -> (Int, a)
random (min,max) x = (z, c z * ((max - min)/c m) + min)
  where z = a * x `mod` m :: Int
        a = 16807 :: Int
        m = 2147483647 :: Int
        c = fromRational . toRational

ballRow :: Int -> Float -> Float -> Float -> Float -> Int -> [PhysicsObject]
ballRow 0 _ _ _ _ _ = []
ballRow nx xsep x y vmax rnum = defaultPhysicsObject{ pObjId = objId
                                          , mass=1
                                          , position=Vec3(x,y,0)
                                          , velocity=Vec3(r1,r2,0)
                                          , mesh = regularPolygon 10 30} : ballRow (nx-1) xsep (x+xsep) y vmax rnum''
  where objId = "ball_" ++ show x ++ "_" ++ show y
        (rnum', r1) = random (-vmax,vmax) rnum
        (rnum'', r2) = random (-vmax,vmax) rnum'

ballGrid :: Int -> Int -> Float -> Float -> Float -> Float -> Float -> Int -> [PhysicsObject]
ballGrid _ 0 _ _ _ _ _ _ = []
ballGrid nx ny xsep ysep x y vmax rnum = ballRow nx xsep x y vmax rnum ++ ballGrid nx (ny-1) xsep ysep x (y+ysep) vmax (rnum+1)

ball1 = defaultPhysicsObject{ pObjId="Ball1"
                            , mass = 1
                            , position=Vec3 (0, 100, 0)
                            , velocity=Vec3 (0, -100, 0)
                            , mesh = regularPolygon 10 10}

ball2 = defaultPhysicsObject{ pObjId="Ball2"
                            , mass = 1
                            , position=Vec3 (0, 0, 0)
                            , velocity=Vec3 (0, 0, 0)
                            , mesh = regularPolygon 10 10}

boundBox = defaultPhysicsObject{ pObjId="BoundBox"
                               , dynamic=False
                               , mesh = box 800 800}

ball3 = defaultPhysicsObject{ pObjId="Ball3"
                            , mass = 0.5
                            , position=Vec3 (450, 0, 0)
                            , velocity=Vec3 (0, -50, 0)
                            , mesh = regularPolygon 8 20}

