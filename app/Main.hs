import Graphics.Gloss
import Graphics.Gloss.Data.ViewPort
import World

windowDisplay :: Display
windowDisplay = InWindow "Window" (200, 200) (10, 10)

main :: IO()
main = simulate
  windowDisplay
  white
  simulationRate
  initialWorld
  drawingFunc
  updateFunc

  where
    simulationRate :: Int
    simulationRate = 60
    
    initialWorld :: World
    initialWorld = testWorld

    drawingFunc :: World -> Picture
    drawingFunc = drawWorld

    updateFunc :: ViewPort -> Float -> World -> World
    updateFunc _ = updateWorld

