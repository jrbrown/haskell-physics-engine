{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_physics_engine (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/jasonb/programming/Haskell/physics_engine/.stack-work/install/x86_64-linux-tinfo6/7ff47e392c6ef07f8011203a5345da7d1b47250bc7f2d8b27d7db066b45b0b89/8.10.7/bin"
libdir     = "/home/jasonb/programming/Haskell/physics_engine/.stack-work/install/x86_64-linux-tinfo6/7ff47e392c6ef07f8011203a5345da7d1b47250bc7f2d8b27d7db066b45b0b89/8.10.7/lib/x86_64-linux-ghc-8.10.7/physics-engine-0.1.0.0-3rX1URA9g6CIf9aNWgBMDH-physics-engine-exe"
dynlibdir  = "/home/jasonb/programming/Haskell/physics_engine/.stack-work/install/x86_64-linux-tinfo6/7ff47e392c6ef07f8011203a5345da7d1b47250bc7f2d8b27d7db066b45b0b89/8.10.7/lib/x86_64-linux-ghc-8.10.7"
datadir    = "/home/jasonb/programming/Haskell/physics_engine/.stack-work/install/x86_64-linux-tinfo6/7ff47e392c6ef07f8011203a5345da7d1b47250bc7f2d8b27d7db066b45b0b89/8.10.7/share/x86_64-linux-ghc-8.10.7/physics-engine-0.1.0.0"
libexecdir = "/home/jasonb/programming/Haskell/physics_engine/.stack-work/install/x86_64-linux-tinfo6/7ff47e392c6ef07f8011203a5345da7d1b47250bc7f2d8b27d7db066b45b0b89/8.10.7/libexec/x86_64-linux-ghc-8.10.7/physics-engine-0.1.0.0"
sysconfdir = "/home/jasonb/programming/Haskell/physics_engine/.stack-work/install/x86_64-linux-tinfo6/7ff47e392c6ef07f8011203a5345da7d1b47250bc7f2d8b27d7db066b45b0b89/8.10.7/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "physics_engine_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "physics_engine_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "physics_engine_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "physics_engine_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "physics_engine_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "physics_engine_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
